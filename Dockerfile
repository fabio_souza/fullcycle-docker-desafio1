FROM golang:alpine as builder

WORKDIR /usr/src/app

COPY fullcycle.go .

RUN go build fullcycle.go

FROM scratch

WORKDIR /usr/src/app

COPY --from=builder /usr/src/app .

CMD [ "./fullcycle" ]